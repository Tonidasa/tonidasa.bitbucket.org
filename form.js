
//This function add more fields to add more books
function addMore(){
	var newBook = document.getElementById("fmBooks").innerHTML;
	var books = document.getElementById("moreBooks").innerHTML;
	document.getElementById("moreBooks").innerHTML = books + newBook; 
}

//This function validate the compulsory fields
//Note that books fields are not compulsory
function checkFields(){
	var result = true;
	var identifiers =	["name", "surname", "address", "zipcd", "city", "country", "email", "phone"];
	for(i = 0; i < identifiers.length; i++){
		var a = document.getElementById(identifiers[i])	
		if (a.value == "") {	
			result = false;
			a.style.border = "thin solid red";
		} else {
			a.style.border = "initial";
		}
	}
	if (!result) {
		document.getElementById("warning").style.display = 'block';
	}
	return result;
	
}

function initialize() {
  var mapProp = {
    center:new google.maps.LatLng(53.348761,-6.243137),
    zoom:17,
    mapTypeId:google.maps.MapTypeId.ROADMAP
  };
  var map=new google.maps.Map(document.getElementById("googleMap"),mapProp);
}
google.maps.event.addDomListener(window, 'load', initialize);